package com.example.covidmaps.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import androidx.core.app.NotificationCompat;
import com.example.covidmaps.R;

public class Notifications extends Service {
    public static final String NOTIFICATION_CHANNEL_ID = "10001" ;
    private final static String default_notification_channel_id = "default" ;

    @Override
    public IBinder onBind (Intent arg0) {
        return null;
    }
    @Override
    public int onStartCommand (Intent intent , int flags , int startId) {
        super .onStartCommand(intent , flags , startId) ;
        createNotification();
        return START_STICKY ;
    }

    private void createNotification () {
        Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_launcher);

        NotificationManager notification = (NotificationManager)this.getSystemService(this.NOTIFICATION_SERVICE);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),default_notification_channel_id);
        builder .setContentTitle("COVID-19 Maps")
                .setContentText("กดเลือกพื้นทีจังหวัดเพื่อดูข้อมูล")
                .setSmallIcon(R.drawable.ic_warning_black_24dp)
                .setLargeIcon(bitmap)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis());

        if (android.os.Build.VERSION. SDK_INT >= android.os.Build.VERSION_CODES. O ) {
            int importance = NotificationManager. IMPORTANCE_HIGH ;
            NotificationChannel notificationChannel = new NotificationChannel( NOTIFICATION_CHANNEL_ID , "NOTIFICATION_CHANNEL_NAME" , importance) ;
            builder.setChannelId( NOTIFICATION_CHANNEL_ID ) ;
            assert notification != null;
            notification.createNotificationChannel(notificationChannel) ;
        }

        notification.notify(1000,builder.build()) ;
    }
}
