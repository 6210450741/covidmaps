package com.example.covidmaps.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.example.covidmaps.R;

public class NewsActivity extends AppCompatActivity {

    Button home;
    WebView webView;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        home = findViewById(R.id.home5);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(NewsActivity.this,MainActivity.class);
                startActivity(home);
            }
        });

        loadNews();
    }
    private void loadNews(){
        webView = findViewById(R.id.webview);
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                loadingAnimation();
            }
            @Override
            public void onPageFinished(WebView view, String url) {
                onBackPressed();
            }
        });
        webView.loadUrl("https://www.khaosod.co.th/covid-19");
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDisplayZoomControls(true);
    }

    private void loadingAnimation(){
        progressDialog = new ProgressDialog(NewsActivity.this);
        progressDialog.show();
        progressDialog.setContentView(R.layout.loading);
        progressDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
        progressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public void onBackPressed() {
        progressDialog.dismiss();
    }
}
