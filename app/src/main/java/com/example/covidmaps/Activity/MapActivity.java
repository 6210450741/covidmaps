package com.example.covidmaps.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.example.covidmaps.R;
import com.example.covidmaps.services.Notifications;

public class MapActivity extends AppCompatActivity {

    Button home;
    WebView Map;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        home = findViewById(R.id.home3);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(MapActivity.this, MainActivity.class);
                startActivity(home);
            }
        });
        loadMap();
    }
    private void loadMap(){
        Map = findViewById(R.id.webviewMap);
        Map.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                loadingAnimation();
            }
            @Override
            public void onPageFinished(WebView view, String url) {
                startService(new Intent(getApplicationContext(), Notifications.class));
                onBackPressed();
            }
        });
        Map.loadUrl("https://covid19.th-stat.com/th/share/map");
        WebSettings webSettings = Map.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDisplayZoomControls(true);
    }
    private void loadingAnimation(){
        progressDialog = new ProgressDialog(MapActivity.this);
        progressDialog.show();
        progressDialog.setContentView(R.layout.loading);
        progressDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
        progressDialog.setCanceledOnTouchOutside(false);
    }
    @Override
    public void onBackPressed() {
        progressDialog.dismiss();
    }
}
