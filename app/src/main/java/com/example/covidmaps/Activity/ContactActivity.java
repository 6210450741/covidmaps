package com.example.covidmaps.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.example.covidmaps.R;

public class ContactActivity extends AppCompatActivity {

    Button home;
    TextView t1,t2,t3,t4,t5,t6,t7,t8,t9;
    String s1,s2,s3,s4,s5,s6,s7,s8,s9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        home = findViewById(R.id.home2);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(ContactActivity.this,MainActivity.class);
                startActivity(home);
            }
        });
        setCall();
    }
    private void setCall(){
        t1 = findViewById(R.id.textView2);t2 = findViewById(R.id.textView3);t3 = findViewById(R.id.textView5);
        t4 = findViewById(R.id.textView6);t5 = findViewById(R.id.textView7);t6 = findViewById(R.id.textView8);
        t7 = findViewById(R.id.textView9);t8 = findViewById(R.id.textView10);t9 = findViewById(R.id.textView11);

        s1 = "1422 กรมควบคุมโรค";
        s2 = "1330 สำนักงานหลักประกันสุขภาพแห่งชาติ";
        s3 = "1668 กรมการแพทย์";
        s4 = "1669 สถาบันการแพทย์ฉุกเฉินแห่งชาติ";
        s5 = "1111 ศูนย์บริการข้อมูลภาครัฐเพื่อประชาชน";
        s6 = "1646 ศูนย์บริการแพทย์ฉุกเฉิน (ศูนย์เอราวัณ)";
        s7 = "0-2245-4964 สายด่วนสำนักอนามัย";
        s8 = "1323 กรมสุขภาพจิต";
        s9 = "0-2193-7057 กรมสนับสนุนบริการสุขภาพ";

        SpannableString ss1 = new SpannableString(s1);
        SpannableString ss2 = new SpannableString(s2);
        SpannableString ss3 = new SpannableString(s3);
        SpannableString ss4 = new SpannableString(s4);
        SpannableString ss5 = new SpannableString(s5);
        SpannableString ss6 = new SpannableString(s6);
        SpannableString ss7 = new SpannableString(s7);
        SpannableString ss8 = new SpannableString(s8);
        SpannableString ss9 = new SpannableString(s9);

        ClickableSpan click_1 = new ClickableSpan() {

            @Override
            public void onClick(@NonNull View widget) {
                String num = "1422";
                String s = "tel:" + num;
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(s));
                startActivity(intent);
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.BLUE);
            }
        };
        ClickableSpan click_2 = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                String num = "1330";
                String s = "tel:" + num;
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(s));
                startActivity(intent);
            }
            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.BLUE);
            }
        };
        ClickableSpan click_3 = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                String num = "1668";
                String s = "tel:" + num;
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(s));
                startActivity(intent);
            }
            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.BLUE);
            }
        };
        ClickableSpan click_4 = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                String num = "1669";
                String s = "tel:" + num;
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(s));
                startActivity(intent);
            }
            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.BLUE);
            }
        };
        ClickableSpan click_5 = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                String num = "1111";
                String s = "tel:" + num;
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(s));
                startActivity(intent);
            }
            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.BLUE);
            }
        };
        ClickableSpan click_6 = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                String num = "1646";
                String s = "tel:" + num;
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(s));
                startActivity(intent);
            }
            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.BLUE);
            }
        };
        ClickableSpan click_7 = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                String num = "022454964";
                String s = "tel:" + num;
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(s));
                startActivity(intent);
            }
            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.BLUE);
            }
        };
        ClickableSpan click_8 = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                String num = "1323";
                String s = "tel:" + num;
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(s));
                startActivity(intent);
            }
            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.BLUE);
            }
        };
        ClickableSpan click_9 = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                String num = "021937057";
                String s = "tel:" + num;
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(s));
                startActivity(intent);
            }
            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.BLUE);
            }
        };
        ss1.setSpan(click_1,0,4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss2.setSpan(click_2,0,4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss3.setSpan(click_3,0,4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss4.setSpan(click_4,0,4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss5.setSpan(click_5,0,4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss6.setSpan(click_6,0,4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss7.setSpan(click_7,0,11, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss8.setSpan(click_8,0,4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss9.setSpan(click_9,0,11, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        t1.setText(ss1);
        t1.setMovementMethod(LinkMovementMethod.getInstance());
        t2.setText(ss2);
        t2.setMovementMethod(LinkMovementMethod.getInstance());
        t3.setText(ss3);
        t3.setMovementMethod(LinkMovementMethod.getInstance());
        t4.setText(ss4);
        t4.setMovementMethod(LinkMovementMethod.getInstance());
        t5.setText(ss5);
        t5.setMovementMethod(LinkMovementMethod.getInstance());
        t6.setText(ss6);
        t6.setMovementMethod(LinkMovementMethod.getInstance());
        t7.setText(ss7);
        t7.setMovementMethod(LinkMovementMethod.getInstance());
        t8.setText(ss8);
        t8.setMovementMethod(LinkMovementMethod.getInstance());
        t9.setText(ss9);
        t9.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
