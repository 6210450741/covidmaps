package com.example.covidmaps.Activity;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.android.volley.*;
import com.android.volley.toolbox.*;
import com.example.covidmaps.R;
import com.example.covidmaps.services.MySingleton;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    TextView total, regen, dead, today, date;
    Button map, news;
    ImageButton contact;
    final String[] dataToday = new String[2];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        contact = findViewById(R.id.imageButton);
        map = findViewById(R.id.maps);
        news = findViewById(R.id.news);
        total = findViewById(R.id.totalNum);
        regen = findViewById(R.id.regenNum);
        dead = findViewById(R.id.deadNum);
        today = findViewById(R.id.numOf);
        date = findViewById(R.id.date);

        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent map = new Intent(MainActivity.this,MapActivity.class);
                startActivity(map);
            }
        });
        news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newS = new Intent(MainActivity.this,NewsActivity.class);
                startActivity(newS);
            }
        });
        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent contacts = new Intent(MainActivity.this,ContactActivity.class);
                startActivity(contacts);
            }
        });
        getData();
    }
    private void getData() {
        String url = "https://covid19.th-stat.com/api/open/today";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,url, null,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            dataToday[0] = response.getString("NewConfirmed");
                            total.setText(response.getString("Confirmed"));
                            dead.setText(response.getString("Deaths"));
                            regen.setText(response.getString("Recovered"));
                            today.setText(response.getString("NewConfirmed"));
                            date.setText(response.getString("UpdateDate"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                today.setText("error");
            }
        });
        MySingleton.getInstance(this).addToRequestQueue(request);
    }
}
